import pandas as pd

data=pd.read_csv('fyx_chinamoney.csv')
data_list=data.values.tolist()
batch_size=80
batch_num=500//batch_size + 1
batches = [[] for _ in range(batch_num)]
for i in range(batch_num):
    batches[i].append(data_list[i*80:i*80+80])
    print(f'批次{i+1}',batches[i])