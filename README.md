# spider_demo

#### 介绍
https://iftp.chinamoney.com.cn/english/bdInfo/
1. 从链接页面获取公开数据
2. 需要获取数据的条件: Bond Type=Treasury Bond, Issue Year=2023
3. 解析返回表格数据，列名包括ISIN, Bond Code, Issuer, Bond Type, Issue Date, Latest Rating
3. 保存成有效csv文件，

### 代码
#### 题目一
文件：dmeo.py是爬虫文件

requests+csv+json转化

返回的数据无法直接操作，需要使用json转为python认为的json格式

parse函数是用来对数据的解析

save函数是用来对数据的保存csv文件

#### 题目二
文件：preticy.py是编程题文件

使用了panads来读取csv数据，进行数据的拆分

一个代码列表长度500，要求以80个为一个批次，拆分成多个数组打印输出，测试数据来源
